<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Link;
use yii\console\Controller;
use PHPHtmlParser\Dom;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class IndexController extends Controller
{
    /**
     * This command echoes what you have entered as the url.
     * @param string $url the url to be echoed.
     */
    public function actionIndex($url)
    {
        echo 'Processing URL: "' . $url . "\"\n\n";

        $dom = new Dom;
        $dom->load($url);

        $links = $dom->find('a.product');

        foreach ($links as $link) {
            $model = new Link();
            $model->link = $link->getAttribute('href');
            $model->save(false);
        }

        echo 'Links (' . count($links) . ') are now in the DB';
    }
}
